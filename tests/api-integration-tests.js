var assert = require('assert');
var superagent = require('superagent');
var express = require('express');
var status = require('http-status');
 
var config = {};
describe('/user', function() {
  var app = express();
  
  var server = app.listen( 3001 );
 
  after(function() {
    console.log('User public route test completed');
    server.close();
  });

  // User creation
  it('returns 200 if TestUser is succesfully created', function(done) {
    superagent.post('http://localhost:3000/user')
      .send({ username: 'TestUser', password: 'TestPassword'})
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .end(function(err, res) {
      assert.ifError(err);
      assert.equal(res.status, status.OK);
      done();
    });
  });

  // User creation with a name that exists
  it('returns 201 if TestUser already exists', function(done) {
    superagent.post('http://localhost:3000/user')
      .send({ username: 'TestUser', password: 'TestPassword'})
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .end(function(err, res) {
      assert.ifError(err);
      assert.equal(res.status, 201);
      done();
    });
  });

  // User login
  it('returns 200 and an api key if login is valid', function(done) {
    superagent.post('http://localhost:3000/login')
      .send({ username: 'TestUser', password: 'TestPassword'})
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .use( setConfigVar )
      .end(function(err, res) {
      assert.ifError(err);
      assert.equal(res.status, status.OK);
      assert( res.body.token );
      setConfigVar( 'token', res.body.token );
      done();
    });
  });
 
});

describe('/api/v1/lunch', function() {
  var app = express();
  
  var server = app.listen( 3001 );
 
  after(function() {
    console.log('Lunch tests completed');
  });

  // List of lunches
  it('returns 200 if the user can GET a list of lunches', function(done){
    superagent.get('http://localhost:3000/api/v1/lunches')
    .send({username: 'TestUser'})
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', getConfigVar( 'token' ) )
    .set('x-key', 'TestUser')
    .end(function(err, res){
      assert.ifError(err);
      assert.equal(res.status, status.OK);
      done();
    });
  });

  // Posting a new lunch
  it('returns 200 if the user can POST a new lunch item', function(done){
    superagent.post('http://localhost:3000/api/v1/lunches')
    .send({username: 'TestUser', password: 'TestPassword', amount: 8, location: '123123123', locationName: 'TestLunch'})
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', getConfigVar( 'token' ) )
    .set('x-key', 'TestUser')
    .use( setConfigVar )
    .end(function(err, res){
      assert.ifError(err);
      assert.equal(res.status, status.OK);
      setConfigVar( 'lunchID', res.body.id )
      done();
    });
  });

  // Looking for a lunch that doesnt exist
  it('returns 404 if the requested lunch is not found', function(done){
    superagent.get('http://localhost:3000/api/v1/lunches/5659a090fdbec2d5346f9738' )
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', getConfigVar( 'token' ) )
    .set('x-key', 'TestUser')
    .end(function(err, res){
      assert.equal(res.status, status.NOT_FOUND);
      done();
    });
  });

  // Getting a single lunch
  it('returns 200 if the user can GET a single lunch', function(done){
    superagent.get('http://localhost:3000/api/v1/lunches/' + getConfigVar( 'lunchID' ) )
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', getConfigVar( 'token' ) )
    .set('x-key', 'TestUser')
    .end(function(err, res){
      assert.equal(res.status, status.OK);
      done();
    });
  });

});

describe('/api/admin/v1/restaurantgroup', function(){
  var app = express();
  var server = app.listen( 3001 );

  after(function() {
    console.log('Admin restaurant group tests completed');
  });

  it('returns 200 if admin can succesfully get restaurant groups', function( done ){
    // testadmin
    superagent.get('http://localhost:3000/api/admin/v1/restaurantgroup')
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MzY0MTQ2MzkzMzksInVzZXIiOiJsdW5jaHN0ZXJkZXYifQ.gYWu-fq1iv4eLhqbCOiLFlf1EFr4KhiCxX03LnghmKU" )
    .set('x-key', 'lunchsterdev')
    .end( function( err, res ) {
      assert.ifError( err );
      assert.equal( res.status, status.OK );
      done();
    });
  });

  it('returns 401 if regular user will fail in GETting restaurant groups', function( done ){
    superagent.get('http://localhost:3000/api/admin/v1/restaurantgroup')
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MzU5OTM2ODQ0OTksInVzZXIiOiJ0ZXN0am9obmRvZSJ9.G_487Uodr9M6AksEOuwyAF_-WQfWAlkkKSMRfuI8Dtc" )
    .set('x-key', 'testjohndoe')
    .end( function( err, res ) {
      assert.equal( res.status, status.UNAUTHORIZED );
      done();
    });
  });

   it('returns 200 if admin can succesfully create restaurant groups', function( done ){
    // testadmin tokens
    superagent.post('http://localhost:3000/api/admin/v1/restaurantgroup')
    .send({name: "Latokartanon testikeittiöstöt"})
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MzY0MTQ2MzkzMzksInVzZXIiOiJsdW5jaHN0ZXJkZXYifQ.gYWu-fq1iv4eLhqbCOiLFlf1EFr4KhiCxX03LnghmKU" )
    .set('x-key', 'lunchsterdev')
    .end( function( err, res ) {
      assert.ifError( err );
      assert.equal( res.status, status.OK );
      done();
    });
  });
});

describe('/api/admin/v1/restaurant', function(){

  var app = express();
  var server = app.listen( 3001 );

  it('returns 200 if admin succesfully create restaurants', function( done ){
    superagent.post('http://localhost:3000/api/admin/v1/restaurant')
      .send({ name: "Testikeittiö", 
              latitude: 60.1698626,
              longitude: 24.938378699999998,
              openingHours: { opens: "10.00", closes: "22.00"} }
              )
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .set('x-access-token', "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MzY0MTQ2MzkzMzksInVzZXIiOiJsdW5jaHN0ZXJkZXYifQ.gYWu-fq1iv4eLhqbCOiLFlf1EFr4KhiCxX03LnghmKU" )
      .set('x-key', 'lunchsterdev')
      .use( setConfigVar )
      .end( function( err, res ){
       
        assert.ifError( err );
        assert.equal( res.status, status.OK );

        if( !res.body.restaurant )
          assert.fail( res.body.restaurant, {}, 'Expected a restaurant object but got undefined', '==' );

        setConfigVar( 'restaurantID', res.body.restaurant._id)
        done();
      });
    });
});

describe('/api/v1/restaurant', function(){

  var app = express();
  var server = app.listen( 3001 );

  after(function() {
    console.log('Restaurant public tests completed');
  });

  it('returns 200 if user can succesfully GET restaurants', function( done ){
    superagent.get('http://localhost:3000/api/v1/restaurants')
     .set('Accept', 'application/json')
     .set('Content-type', 'application/json')
     .set('x-access-token', getConfigVar( 'token' ) )
     .set('x-key', 'TestUser')
     .end( function( err, res ){
        assert.ifError( err );
        assert.equal( res.status, status.OK );
        done();
     });
  });

  it('returns 200 if user can succesfully GET a single restaurant', function( done ){
    superagent.get('http://localhost:3000/api/v1/restaurant/' + getConfigVar( 'restaurantID' ) )
     .set('Accept', 'application/json')
     .set('Content-type', 'application/json')
     .set('x-access-token', getConfigVar( 'token' ) )
     .set('x-key', 'TestUser')
     .end( function( err, res ){
        assert.ifError( err );
        assert.equal( res.status, status.OK );
        done();
     });
  });

});

describe('/api/admin/v1/offer', function(){

  var app = express();
  var server = app.listen( 3001 );

  after(function() {
    console.log('Offer admin tests completed');
  });

  it( 'returns 200 if admin can succesfully POST an offer', function( done ){

    superagent.post( 'http://localhost:3000/api/admin/v1/offer' )
      .send({ title: 'Testitarjous', description: 'Testitarjouksen selostus. Testitarjouksessa saat testitarjouksen testitarjouksen puoleen hintaan.', validUntil: '2016-01-30', usageRestriction: 10, restaurantID: getConfigVar( 'restaurantID' )})
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .set('x-access-token', "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MzY0MTQ2MzkzMzksInVzZXIiOiJsdW5jaHN0ZXJkZXYifQ.gYWu-fq1iv4eLhqbCOiLFlf1EFr4KhiCxX03LnghmKU" )
      .set('x-key', 'lunchsterdev')
      .use( setConfigVar )
      .end( function( err, res ){

        assert.ifError( err );
        assert.equal( res.status, status.OK );

        if( !res.body.offer )
          assert.fail( res.body.offer, 'Offer object', 'Expected an offer object but got undefined or null', '==' );

        if( res.body.offer._id )
          setConfigVar( 'offerID', res.body.offer._id );

        done();
      });
  }); 
});

describe('/api/v1/offer', function(){

  var app = express();
  var server = app.listen( 3001 );

  after(function() {
    console.log('Offer public tests completed');
  });

  it( 'returns 200 if user can succesfully GET offers', function( done ){
  superagent.get( 'http://localhost:3000/api/v1/offers' )
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .set('x-access-token', getConfigVar( 'token' ) )
      .set('x-key', 'TestUser')
      .use( setConfigVar )
      .end( function( err, res ){

        assert.equal( res.status, status.OK );

        if( !res.body.offers )
          assert.fail( res.body.offers, 'Offers object', 'Expected an offers object but got undefined or null', '==' );

        done();
      });
   });

   it( 'returns 200 if user can succesfully GET offers', function( done ){
   superagent.get( 'http://localhost:3000/api/v1/offer/' + getConfigVar( 'offerID' ))
      .set('Accept', 'application/json')
      .set('Content-type', 'application/json')
      .set('x-access-token', getConfigVar( 'token' ) )
      .set('x-key', 'TestUser')
      .end( function( err, res ){

        assert.ifError( err );
        assert.equal( res.status, status.OK );

        if( !res.body.offer )
          assert.fail( res.body.offer, 'Offer object', 'Expected an offer object but got undefined or null', '==' );

        done();
      });    
    });
});

// Always keep this block as last, as most requests need the authorization 
describe('/api/v1/user', function(){

  var app = express();
  var server = app.listen( 3001 );
 
  after(function() {
    console.log('User operations requiring authorization tests completed');
  });

    // User unauthorized deletion
  it('returns 401 if an delete attempt is made without a proper token', function(done){
    superagent.del('http://localhost:3000/api/v1/user')
    .send({username: 'TestUser'})
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-key', 'TestUser')
    .end(function(err, res){
      assert.equal(res.status, status.UNAUTHORIZED);
      done();
    });
  });

  // User deletion
  it('returns 200 if the user is succesfully deleted', function(done){
    superagent.del('http://localhost:3000/api/v1/user')
    .send({username: 'TestUser'})
    .set('Accept', 'application/json')
    .set('Content-type', 'application/json')
    .set('x-access-token', getConfigVar( 'token' ) )
    .set('x-key', 'TestUser')
    .use( getConfigVar )
    .end(function(err, res){
      assert.ifError(err);
      assert.equal(res.status, status.OK);
      done();
    });
  });
 
});


// This is a bit ugly, but effective... better way?
function setConfigVar( key, val ){
  config[key] = val;
}

function getConfigVar( key ){
  return config[key];
}