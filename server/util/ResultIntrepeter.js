var conf = require('../config/config.js');

var BadRequestError = require( './error/BadRequestError' );
var InternalServerError = require( './error/InternalServerError' );
var ResourceExistsError = require( './error/ResourceExistsError' ); 
var ResourceNotFoundError = require( './error/ResourceNotFoundError' ); 
var InvalidCredentialsError = require( './error/InvalidCredentialsError' ); 

var ResultIntrepeter = {

	resolve: function( result, response, success ){

			if( result instanceof InternalServerError ){
				response.status( 500 );
				response.json( conf.Response.serverError );
			}
			else if( result instanceof BadRequestError ){
				response.status( 400 );
				response.json( conf.Response.badRequest( result.message ) );
			}
			else if( result instanceof InvalidCredentialsError ){
				response.status( 401 );
				response.json( conf.Response.invalidCredentials );
			}
			else if( result instanceof ResourceNotFoundError ){
				response.status( 404 );
				response.json( conf.Response.resourceNotFound( result.message ) );
			}
			else if( result instanceof ResourceExistsError ){
				response.status( 201 );
				response.json( conf.Response.resourceExists( result.message ) );
			}
			else return success();
	}
}

module.exports = ResultIntrepeter;