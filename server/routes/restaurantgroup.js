var DP = require('../odm/DataPool.js');
var conf = require('../config/config.js');

var ResultIntrepeter = require( '../util/ResultIntrepeter' );

module.exports = {

	create: function( req, res ){

		DP.RestaurantGroup.create( req.body, function( result ){

			ResultIntrepeter.resolve( result, res, function(){
				res.status( 200 );
				res.json( conf.Response.restaurantGroupOK( result ));
			});
		});
	},

	find: function( req, res ){

		DP.RestaurantGroup.find( function( result ) {

			ResultIntrepeter.resolve( result, res, function(){
				res.status( 200 );
				res.json( conf.Response.restaurantGroupOK( result ) );
			});

		});
	},

	findOne: function( req, res ){

		DP.RestaurantGroup.findOne( req.params, function( result ){

			ResultIntrepeter.resolve( result, res, function(){
				res.status( 200 );
				res.json( conf.Response.restaurantGroupOK( result ) );
			});
		});
	}/*,

	update: function( req, res ){

		DP.RestaurantGroup.update( req.body, function( result ){


		});
	}*/
}