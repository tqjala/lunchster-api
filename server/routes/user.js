var DP = require('../odm/DataPool.js');
var conf = require('../config/config.js');
var ResultIntrepeter = require( '../util/ResultIntrepeter' );

module.exports = {

	create: function( req, res ){

		DP.User.create( req.body.username, req.body.password, function( result ){

		ResultIntrepeter.resolve( result, res, function(){
			
				res.status(200);
				res.json( conf.Response.userCreated );

			});
		});
	},
	remove: function( req, res ){

		DP.User.delete( req.body.username, function( result ){
			
			ResultIntrepeter.resolve( result, res, function(){

				res.status( 200 );
				res.json( conf.Response.userDeleted );

			});
			
		});
	}

}