var express = require('express');
var router = express.Router();

var auth = require('./auth');
var user = require('./user');
var Lunch = require('./lunch');
var RG = require('./restaurantgroup');
var Restaurant = require('./restaurant');
var Offer = require( './offer' );

// Login route
router.post('/user', user.create );
router.post('/login', auth.login );

// Routes for visits
router.get( '/api/v1/lunches', Lunch.findAll );
router.get( '/api/v1/lunches/:id', Lunch.findOne );
router.post( '/api/v1/lunches', Lunch.create );

router.get( '/api/v1/restaurants', Restaurant.find );
router.get( '/api/v1/restaurant/:id', Restaurant.findOne );

router.get( '/api/v1/offers', Offer.find );
router.get( '/api/v1/offer/:id', Offer.findOne );

// User routes which require full auth process
router.delete( '/api/v1/user', user.remove );

// Admin routes
router.post( '/api/admin/v1/restaurantgroup', RG.create );
router.get( '/api/admin/v1/restaurantgroups', RG.find );
router.get( '/api/admin/v1/restaurantgroup/:id', RG.findOne );
//router.get( '/api/admin/v1/restaurantgroup/:id/restaurants', RG.getRestaurantsForGroup );
//router.patch( '/api/admin/v1/restaurantgroup/:id', RestaurantGroup.update );

router.get( '/api/admin/v1/restaurants', Restaurant.find );
router.get( '/api/admin/v1/restaurant/:id', Restaurant.findOne );
router.post( '/api/admin/v1/restaurant', Restaurant.create );

router.get( '/api/admin/v1/offers', Offer.find );
router.get( '/api/admin/v1/offer/:id', Offer.findOne );
router.post( '/api/admin/v1/offer', Offer.create );

module.exports = router;