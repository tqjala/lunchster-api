var DP = require('../odm/DataPool.js');
var ResultIntrepeter = require( '../util/ResultIntrepeter' );

module.exports = {

	create: function( req, res ){

		DP.Lunch.create( req.body, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status(200);
				res.json({"status": 200, "message": "Lunch created", "id": result._id, "amount": result.amount, "location": result.location, "locationName": result.locationName, "date": result.date });

			});
				
		});

	},
	findAll: function( req, res ){
		
		DP.Lunch.findAll( req.query, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status(200);
				res.json({"status": 200, "Lunches": result ? result : {}, "count": result ? result.length : 0 });

			});

		});	
	},
	findOne: function( req, res ){
		
		DP.Lunch.findOne( req.params, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				if( result && result._id ){
					res.status(200);
					res.json({"status": 200, "Lunches": result, "count": result.length });
				}
				else{
					res.status( 404 );
					res.json({"status": 404, "message": "Lunch not found!"});
				}

			});

		});	
			
	}
}