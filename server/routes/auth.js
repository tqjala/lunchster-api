var DP = require('../odm/DataPool.js');
var conf = require('../config/config.js');
var jwt = require('jwt-simple');

var ResultIntrepeter = require( '../util/ResultIntrepeter' );

var auth = {

	login: function( req, res ){

		var user = req.body.username || '';
		var password = req.body.password || '';

	    if (user == '' || password == '') {
	      res.status(401);
	      res.json( conf.Response.invalidCredentials );
	      return;
		}

		DP.User.authenticate( user, password, function( authed ){
			
			ResultIntrepeter.resolve( authed, res, function(){

				res.status( 200 );
				res.json(genToken( authed ));

			});

		});

	},
	validateUser: function( username, next ){

		DP.User.find( username, function( result ){
			
			if( result instanceof Error ){
				next( false );
			}

			else next( result );

		});
	}
}

function genToken( user ){

	// tokens are valid for a thousand days
	// username is hashed in the token
	var expires = expiresIn(1000);
	var token = jwt.encode({ exp: expires, user: user }, require('../config/secret')());

	return { status: 200, token: token, expires: expires, user: user, message: 'Authentication succesful' };
}

function expiresIn( numDays ) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;