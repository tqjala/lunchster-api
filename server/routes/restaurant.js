var DP = require('../odm/DataPool.js');
var conf = require('../config/config.js');

var ResultIntrepeter = require( '../util/ResultIntrepeter' );

module.exports = {

	create: function( req, res ){

		DP.Restaurant.create( req.body, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status( 200 );
				res.json( conf.Response.restaurantOK( result ) );
			});

		});

	},

	find: function( req, res ){

		DP.Restaurant.find( req.query, function( result ){

			ResultIntrepeter.resolve( result, res, function(){
				
				res.status( 200 );
				res.json( conf.Response.restaurantOK( result ) );
			});

		});

	},

	findOne: function( req, res ){
		
		DP.Restaurant.findOne( req.params, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status( 200 );
				res.json( conf.Response.restaurantOK( result ) );
			});

		});

	}
}