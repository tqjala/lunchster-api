var DP = require('../odm/DataPool.js');
var ResultIntrepeter = require( '../util/ResultIntrepeter' );

module.exports = {

	create: function( req, res ){

		DP.Offer.create( req.body, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status( 200 );
				res.json({ "status": 200, "message": "Offer created", "offer": result });
			});

		});
	},
	find: function( req, res ){
		
		DP.Offer.find( req.query, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status( 200 );
				res.json({ "status": 200, "message": "Offer created", "offers": result });
			});

		});
	},
	findOne: function( req, res ){

		DP.Offer.findOne( req.params, function( result ){

			ResultIntrepeter.resolve( result, res, function(){

				res.status( 200 );
				res.json({ "status": 200, "message": "Offer created", "offer": result });
			});

		});
	}
}
