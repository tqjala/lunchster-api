/* Exports Mongoose object handling */ 

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
var validate = require('mongoose-validator');

var BadRequestError = require( '../util/error/BadRequestError' );
var InternalServerError = require( '../util/error/InternalServerError' );
var ResourceExistsError = require( '../util/error/ResourceExistsError' ); 
var ResourceNotFoundError = require( '../util/error/ResourceNotFoundError' ); 
var InvalidCredentialsError = require( '../util/error/InvalidCredentialsError' ); 

mongoose.connect('mongodb://localhost/Lunchr');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Failed to connect'));
db.once('open', function(cb){
	console.log('Connected to db');
});

// Object to be exported
var DataPool = {};

/* Mongoose Utility */

var usernameValidator = [validate({
							validator: 'isLength',
							arguments: [2, 20],
							message: 'Username length should be between {ARGS[0]} and {ARGS[1]} characters'
						}),
						validate({
							validator: 'isAlphanumeric',
							message: 'Username should contain only alpha-numeric characters'
						})];

var amountValidator = [validate({
							validator: 'isNumeric',
							message: 'Amount can contain only numbers'
						})];

var locationNameValidator = [validate({
							validator: 'isLength',
							arguments: [2, 75],
							message: 'Location name length should be between {ARGS[0]} and {ARGS[1]} characters'
						})];

var passwordValidator = [validate({
							validator: 'isLength',
							arguments: [4, 25],
							message: 'Password length should be between {ARGS[0]} and {ARGS[1]} characters'
						})];

/* Lunch schema */
var LunchSchema = mongoose.Schema({

	user: { type: String, required: true, validate: usernameValidator },
	amount: {type: Number, required: true, validate: amountValidator },
	location: {type: String, required: true },
	locationName: {type: String, required: true, validate: locationNameValidator },
	date: {type: Date, required: true }

});

var Lunch = mongoose.model( 'Lunch', LunchSchema );

DataPool.Lunch = {};

DataPool.Lunch.create = function( data, next ){

		var lunch = new Lunch({
			user: data.username,
			amount: data.amount,
			location: data.location,
			locationName: data.locationName,
			date: moment().format('YYYY-MM-DD HH:mm:ss')
		});

		lunch.save( function( fail, obj ){

			if( fail ){
			     return next( new BadRequestError( DataPool.handleSchemaErrors( fail ) ));
			}

			else{
				return next( obj );
			}

		});

}

DataPool.Lunch.findAll = function( data, next ){

    if( data.limit && typeof data.limit != 'number' )
        return next( new BadRequestError( 'Request parameter limit is not of type number' ) );

	var params = {};

    if( data.user )
        params._id = data.user;
	
	Lunch.find( params ).limit( data.limit ? data.limit : 10 ).exec( function( err, Lunches ){

		if( err )
			return next( new InternalServerError() );
		else
			return next( Lunches );

	});

}

DataPool.Lunch.findOne = function( data, next ){

	if( !data.id || data.id == "undefined" )
		return next( new BadRequestError( 'Request is missing parameter id' ));
	
	Lunch.findOne({ "_id": data.id.toString() }, function( err, Lunch ){

		if( err ){
            return next( new InternalServerError() );
		}
		else{
			return next( Lunch );
		}
	});
}

/* User schema */
var userSchema = mongoose.Schema({

	username: { type: String, required: true, validate: usernameValidator },
	password: { type: String, required: true },
	created: { type: Date, required: true },
	level: { type: Number, required: true }

});

var User = mongoose.model( 'User', userSchema );

DataPool.User = {};

DataPool.User.create = function( username, password, next ){

	if( !username )
		return next( new BadRequestError( 'Request is missing parameter username') );

	if( !password )
		return next( new BadRequestError( 'Request is missing parameter password') );

	User.find( {username: username}, function( err, user ){

		// unexpected error
		if( err ){
			return next( new InternalServerError() );
		}
		// username exists
		if( user && user.length != 0 ){
			return next( new ResourceExistsError( 'Username already exists' ) );
		}

		var hash = bcrypt.hashSync( password );

		var newUser = new User({
			username: username,
			password: hash,
			created: moment().format('YYYY-MM-DD HH:mm:ss'),
			level: 0
		});

		newUser.save( function( fail, savedUser ){
				
            if( fail )
                return next( new BadRequestError( DataPool.handleSchemaErrors( fail ) ));

			else
				return next( savedUser );
			

		});

	});
	
}

DataPool.User.authenticate = function( username, password, next ){

	if( !username )
		return next( new BadRequestError( 'Request is missing parameter username') );

	if( !password )
		return next( new BadRequestError( 'Request is missing parameter password') );

	User.findOne( {username: username}, function( err, user ){

		if( err ){
			return next( new InternalServerError() );
		}

		if( !user )
			return next( new ResourceNotFoundError( 'User not found') );

		bcrypt.compare( password, user.password, function( err, matches ){

			if( err ){
				return next( new InternalServerError() );
			}

			if( matches ){
				return next( username );
			}

			else{
				return next( new InvalidCredentialsError() );
                    // TODO: log failed attempts
                }

            });
	})
}

DataPool.User.find = function( username, next ){

	if( !username )
		return next( new BadRequestError( 'Request is missing parameter username') );

	User.findOne( {username: username}, function( err, user ){

		if( err || !user )
			return next( new ResourceNotFoundError( 'User not found' ) );

		else
	        return next( { username: user.username, level: user.level } );

	});
}

DataPool.User.delete = function( username, next ){

	User.findOne( {username: username}, function(err, user){

		if( err ){
			return next( new InternalServerError() );
        }

		if( !user ){
			return next( new ResourceNotFoundError( 'User not found') );
        }

		User.remove( {username: username}, function(err){

			if( err ){
				return next( new InternalServerError() );
            }

			else{
			 	next( true );
			}

		});

	});
}

/* RestaurantGroup schema */
var restaurantGroupSchema = mongoose.Schema({

	name: { type: String, required: true },
	created: { type: Date, required: true }

});

var RestaurantGroup = mongoose.model( 'RestaurantGroup', restaurantGroupSchema );
DataPool.RestaurantGroup = {};

DataPool.RestaurantGroup.create = function( data, next ){

	if( !data.name )
		return next( new BadRequestError( 'Request is missing parameter name') );

	var rg = new RestaurantGroup({ name: data.name, created: moment().format('YYYY-MM-DD HH:mm:ss')});	

	rg.save( function( fail, savedRg ){

		if( fail )
			return next( new BadRequestError( DataPool.handleSchemaErrors( fail ) ));
		else
			return next( savedRg );

	});
}

DataPool.RestaurantGroup.find = function( next ){

	RestaurantGroup.find( {}, function( err, rgs ){

			if( err )
				return next( new InternalServerError() );

			if( !rgs || ( rgs && rgs.length == 0 ) )
				return next( new ResourceNotFoundError( 'RestaurantGroups not found') );
				
			else return next( rgs );

		});
}

DataPool.RestaurantGroup.findOne = function( data, next ){

	if( !data.id || data.id == "undefined" )
		return next( new BadRequestError( 'Request is missing parameter id' ) );

	RestaurantGroup.findOne( { _id: data.id }, function( err, rg ){

		if( err )
			return next( new InternalServerError() );

		if( !rg || ( rg && rg.length == 0 ) )
			return next( new ResourceNotFoundError( 'RestaurantGroup not found') );

		else return next( rg );

	});
}

/* Restaurant schema */
var restaurantSchema = mongoose.Schema({

	name: { type: String, required: true },
	geo: { 
		latitude: { type: Number, required: true },
		longitude: { type: Number, required: true }
	},
	openingHours: { 
		opens: { type: String, required: true },
		closes: { type: String, required: true } },
	description: { type: String },
	groupID: { type: mongoose.Schema.Types.ObjectId, ref: 'RestaurantGroup' }
});

var Restaurant = mongoose.model( 'Restaurant', restaurantSchema );

DataPool.Restaurant = {};

DataPool.Restaurant.create = function( data, next ){

	if( !data.name )
		return next( new BadRequestError( 'Request is missing parameter name' ) );

	if( !data.latitude || !data.longitude )
		return next( new BadRequestError( 'Request is missing geolocation parameters') );

	if( !data.openingHours || !data.openingHours.opens || !data.openingHours.closes )
		return next ( new BadRequestError( 'Request is missing parameter openingHours or its properties') );

	var restaurant = new Restaurant({

		name: data.name,
		geo: { latitude: data.latitude, longitude: data.longitude },
		openingHours: { opens: data.openingHours.opens, closes: data.openingHours.closes },
		description: data.description ? data.description : null,
		groupID: data.group ? data.group : null

	});

	restaurant.save( function( fail, savedRestaurant ){

		if( fail )
			return next( new BadRequestError( DataPool.handleSchemaErrors( fail ) ));

		else return next( savedRestaurant );

	});

};

DataPool.Restaurant.find = function( data, next ){

	var query = {};

	if( data.geo )
		query.geo = data.geo;

	if( data.name )
		query.name = data.name;

	Restaurant.find( query, function( err, restaurants ){

		if( err )
			return next( new InternalServerError() );

		if( !restaurants || ( restaurants && restaurants.length == 0 ) )
			return next( new ResourceNotFoundError( 'No restaurants found') );

		else return next( restaurants );
	});
}

DataPool.Restaurant.findOne = function( data, next ){

	if( !data.id || data.id == "undefined" )
		return next( new BadRequestError( 'Request is missing parameter id ') );

	Restaurant.findOne( { "_id": data.id.toString() }, function( err, restaurant ){

		if( err )
			return next( new InternalServerError() );

		if( !restaurant || ( restaurant && restaurant.length == 0 ) )
			return next( new ResourceNotFoundError( 'Restaurant not found') );

		else return next( restaurant );

	})
}

/* Offer schema */
var offerSchema = mongoose.Schema({

	title: { type: String, required: true },
	description: { type: String, required: true },
	created: { type: Date, required: true },
	validUntil: { type: Date, required: true },
	usageRestriction: { type: Number, required: true, min: 1 },
	restaurantID: { type: mongoose.Schema.Types.ObjectId, ref: 'Restaurant' }
}); 

var Offer = mongoose.model( 'Offer', offerSchema );

DataPool.Offer = {

	create: function( data, next ){

		if( !data.title || typeof data.title == "undefined" )
			return next( new BadRequestError( 'Request is missing parameter title') );

		if( !data.description || typeof data.description == "undefined" )
			return next( new BadRequestError( 'Request is missing parameter description') );

		if( !data.validUntil || typeof data.validUntil == "undefined" )
			return next( new BadRequestError( 'Request is missing parameter validUntil') );

		if( !data.usageRestriction || typeof data.usageRestriction == "undefined" )
			return next( new BadRequestError( 'Request is missing parameter usageRestriction') );

		if( !data.restaurantID || typeof data.restaurantID == "undefined" )
			return next( new BadRequestError( 'Request is missing parameter restaurantID') );

		var offer = new Offer({
			title: data.title,
			description: data.description,
			created: moment().format('YYYY-MM-DD HH:mm:ss'),
			validUntil: data.validUntil,
			usageRestriction: data.usageRestriction,
			restaurantID: data.restaurantID
		});

		offer.save( function( fail, savedOffer ){

			if( fail )
				return next( new BadRequestError( DataPool.handleSchemaErrors( fail ) ));

			else return next ( savedOffer );
		});
	},

	find: function( data, next ){	

		var query = {};

		Offer.find( query, function( err, offers ){

			if( err )
				return next( new InternalServerError() );

			if( !offers || ( offers && offers.length == 0 ) )
				return next( new ResourceNotFoundError( 'No offers found') );

			else return next( offers );

		});

	},

	findOne: function( data, next ){

		if( !data.id || data.id == "undefined" )
			return next( new BadRequestError( 'Request is missing parameter id ') );

		Offer.findOne( { "_id": data.id.toString() }, function( err, offer ){

		if( err )
			return next( new InternalServerError() );

		if( !offer || ( offer && offer.length == 0 ) )
			return next( new ResourceNotFoundError( 'Offer not found') );

		else return next( offer );

		});
	}
};

// TODO: Errors from .save can also be 500's, not just schemaerrors
DataPool.handleSchemaErrors = function( error ){

	errorsToFront = [];

	if( error && error.length != 0 ){
		for( var field in error.errors ){
			errorsToFront.push({field: field, message: error.errors[field].message });
		}
	}

	if( errorsToFront.length > 0 )
		return errorsToFront[0].message;

	else return 'Operation failed';

}

module.exports = DataPool;