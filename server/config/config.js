var Config = {};
var JSONresponse = function( code, message ){
	return { "status": code, "message": message }
}

Response = {};
 
// 2xx
Response.userCreated = JSONresponse( 200, 'User created' );
Response.userDeleted = JSONresponse( 200, 'User deleted' );
Response.restaurantGroupOK = function( rg ){ return {"status": 200, "restaurantGroup": rg }};
Response.restaurantOK = function( restaurant ){ return {"status": 200, "restaurant": restaurant }};
Response.resourceExists = function( msg ){ return JSONresponse( 201, msg ) };

// 4xx
Response.resourceNotFound = function( msg ){ JSONresponse( 404, msg ) };
Response.badRequest = function( msg ){ JSONresponse( 400, msg ) };

Response.tokenExpired = JSONresponse( 400, 'Authentication token expired' );
Response.invalidCredentials = JSONresponse( 401, 'Invalid credentials' );
Response.unauthorized = JSONresponse( 403, 'Unauthorized' );

//5xx
Response.serverError = JSONresponse( 500, 'Internal server error')

Config.Response = Response;
module.exports = Config;