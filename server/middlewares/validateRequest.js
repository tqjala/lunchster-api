var jwt = require('jwt-simple');
var validateUser = require('../routes/auth').validateUser;
var conf = require('../config/config.js');

module.exports = function(req, res, next) {
  
  if(req.method == 'OPTIONS') next();
 
  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  var userAsKey = req.headers['x-key'];
 
  if ( token ) {

    try {
      var decoded = jwt.decode(token, require('../config/secret.js')());
 
      if (decoded.exp <= Date.now()) {
        res.status(400);
        res.json( conf.Response.tokenExpired );
        return;
      } 
        }
      catch (err) {
          res.status(500);
          res.json( conf.Response.serverError );
          return;
      }

      // decode username from the token.
      validateUser( decoded.user, function( user ){
      if (user) {
        // decoded string doesnt match the provided x-key
        if( userAsKey != user.username ){
          res.status(401);
          res.json( conf.Response.invalidCredentials );
          return;
        }

        // possible user param in request must match the decoded
        // otherwise it would be possible to access other users data
        var requestUser = ( req.body && req.body.user ) || ( req.query && req.query.user );

        if( requestUser && ( user.username != requestUser ) ){
          res.status(401);
          res.json( conf.Response.invalidCredentials );
          return;
        }
        if( req.url.indexOf('/api/admin/v1' ) >= 0 ){

          if( user.level >= 4 ){
            next();
          }
          else{  res.status( 401 );
            res.json( conf.Response.unauthorized );
            return;
          }
        }
        // check that user is accessing the allowed resource
        else if ( req.url.indexOf('/api/v1/') >= 0 ) {
          // next here is either next middleware (as defined in the second param of app.all) or the requested endpoint
          next();
        } else {
          res.status(401);
          res.json( conf.Response.unauthorized ); 
          return;
        }
      } else {
        // user not found
        res.status(401);
        res.json( conf.Response.invalidCredentials );
        return;
      }

      }); 

  } else {
    // no token provided, unauthorized
    res.status(401);
    res.json( conf.Response.unauthorized );
    return;
  }
};