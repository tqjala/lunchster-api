### Local environment install instructions

1. Install Node if you dont have it installed, https://nodejs.org/en/
2. Install MongoDB, https://www.mongodb.org/, run mongod.exe --dbpath PATH_TO_YOUR_FAVORITE_FOLDER
(Check that the address in server/odm/DataPool.js:8 is correct for your installation)
3. Navigate to /lunchster-api/server and run npm install
4. In the same folder, run npm start
5. If you see 'Connected to db' in Node console, youre all set!

### Example queries